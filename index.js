/* 1 */

db.users.insertMany([

{
    "_id" : ObjectId("621f268a0ada95273a69f73d"),
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "janedoe@gmail.com"
    },
    "courses" : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    "department" : "HR",
    "isAdmin" : false,
    "status" : "active"
},

// 2
{
    "_id" : ObjectId("621f29170ada95273a69f73e"),
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "stephenhawking@gmail.com"
    },
    "courses" : [ 
        "Python", 
        "React", 
        "PHP"
    ],
    "department" : "HR",
    "status" : "active"
},

// 3
{
    "_id" : ObjectId("621f29170ada95273a69f73f"),
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "neilarmstrong@gmail.com"
    },
    "courses" : [ 
        "React", 
        "Laravel", 
        "Sass"
    ],
    "department" : "HR",
    "status" : "active"
},

//  4
{
    "_id" : ObjectId("621f2c220ada95273a69f740"),
    "firstName" : "Bill",
    "lastName" : "Gates",
    "age" : 65.0,
    "contact" : {
        "phone" : "12345678",
        "email" : "bill@gmail.com"
    },
    "courses" : [ 
        "PHP", 
        "Laravel", 
        "HTML"
    ],
    "department" : "Operations",
    "status" : "active"
}
]
)

// No.2
    
    db.users.find(
        {$or: [{"firstName":{$regex: "s", $options: "i"}}, {"lastName": {$regex: "d", $options: "i"}}]}, 

        {"firstName":1, "lastName":1, "_id":0}

        )
// No. 3
    db.users.find({
        $and: [
        {"department": "HR"},
        ({age: {$gte: 70}})

        ]
    }
    )

// No. 4
    db.users.find({
        $and: [
        {"firstName": {$regex: `e`}},
        ({age: {$lte: 30}})

        ]
    }
    )
